= License Compatibility

License compatibility is a FOSS legal folk doctrine that concerns the
combinability of code covered by different licenses. It has no
specific basis in statutory or judge-made law. Much of the discourse 
around license compatibility is incoherent, contradictory and poorly
reasoned, and rests on dubious interpretive foundations.

Even when relatively clearly formulated, pronouncements on license
compatibility tend to have a restrictive bias. Fedora has long taken a
skeptical view of such pronouncements, which are typically
inconsistent with deeply established packaging traditions and
development practices of community Linux distributions and other FOSS
community projects.

Despite this general standpoint of skepticism, Fedora takes assertions
of license incompatibility seriously. Indeed, in (extremely) rare
cases, packages have been excluded from Fedora Linux because of a
license incompatibility issue. 

If you believe there is a license incompatibility problem of some sort
raised by a Fedora Linux package, we encourage you to
https://bugzilla.redhat.com[file a bug report]. We will investigate
the issue in due course.

TIP: License compatibility issues are off-topic for the
https://gitlab.com/fedora/legal/fedora-license-data[Fedora License
Data] project.

// == Context-dependent nature of license compatibility analysis

// == No general compatibility classifications for allowed licenses

// == License compatibility is largely a GPL issue

// == Equivalence of GPLv3 and GPLv2-or-later

// == Cross-package license compatibility issues generally ignored