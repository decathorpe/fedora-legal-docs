# Fedora legal documentation

[![REUSE status](https://api.reuse.software/badge/gitlab.com/fedora/legal/fedora-legal-docs)](https://api.reuse.software/info/gitlab.com/fedora/legal/fedora-legal-docs)

## License

Documentation in this repository is licensed under the [Creative
Commons Attribution-ShareAlike 4.0 International License](./LICENSES/CC-BY-SA-4.0.txt) (CC BY-SA 4.0). For more information see
.reuse/dep5 and the LICENSES/ directory.

## Contributing

To contribute to this documentation, please submit a merge
request. All copyrightable contributions are licensed under CC BY-SA
4.0.

All contributions made as of 2023-01-28 are covered by the [Developer
Certificate of Origin](./DCO). Inclusion of a `Signed-off-by:` line in
commit messages is highly preferred (you can do this with `git commit
-s` or `git commit --signoff`).

For purposes of applying the DCO, we interpret "the open source
license indicated in the file" as including file-specific license
indications in .reuse/dep5, and we treat CC BY-SA 4.0 (SPDX
`CC-BY-SA-4.0`) as an "open source license" even though it is not an
open source license.


